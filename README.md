# Tic-Tac-Toe - Reinforcement Learning

A second version of Tic-Tac-Toe AI that learns with reinforcement learning. 

## Action Selection

An action selection method would choose appropriately between exploitation and exploration.

This project explores three action selection methods:
1. Epsilon Greedy: 
    * Select random action with probability _ε_
    * Select the best action with probability 1 - _ε_
2. Softmax
    * Choose action _a_ with probability (e^(_Q(s,a)_/_T_)/SUM(e^(_Q(s,a)_/_T_)))
3. Optimistic Utility Exploration
    * _a_ = ARGMAX_f_(_Q(s,a)_,_N(s,a)_)
    * where _f(u,n)_ has values:
        * a positive number if _n_ is less than _N\_e_
        * _u_ if _n_ is big enough

## Reinforcement Learning Algorithm

2 RL algorithms are used in this projects:
1. Active Q-Learning
    * Initialize _Q(s,a)_ to 0 for all _s_, _a_
    * For every step _(s,a)_ the agent takes, update _Q(s,a)_ = _Q(s,a)_ + _α_(_R(s)_ + _𝛾_ MAX(_Q(s',a')_) - _Q(s,a)_))
2. SARSA
    * Initialize _Q(s,a)_ to 0 for all _s_, _a_
    * For every step _(s,a)_ the agent takes, update _Q(s,a)_ = _Q(s,a)_ + _α_(_R(s)_ + _𝛾_ _Q(s',a')_ - _Q(s,a)_))


## Variables Definitions:
* _Q(s,a)_: expected reward performing action _a_ at state _s_
* _N(s,a)_: number of times performing action _a_ at state _s_
* _s_: current state
* _a_: action
* _s'_: next state
* _a'_: next action
* _N\_e_: number of times you want to perform each action at each state
* _R(s)_: reward at the state
* _α_: learning rate
* _𝛾_: discount factor

## Result
| RL Algorithm | Action Selection | Win | Draw | Lose |
|--------------|------------------|-----|------|------|
| SARSA        | softmax          | 980 | 20   | 0    |
| SARSA        | optimistic       | 990 | 10   | 0    |
| SARSA        | epsilon greedy   | 983 | 14   | 3    |
| Q Learning   | softmax          | 985 | 15   | 0    |
| Q Learning   | optimistic       | 998 | 2    | 0    |
| Q Learning   | epsilon greedy   | 983 | 14   | 3    |

The table above shows the result of 1000 games after learning.
We can see that Q-Learning + optimistic works the best, which wins 99.8% of the games.

![rewards vs epochs](images/q_optimistic.PNG "rewards vs epochs")

Observations:
* The Q Learning algorithm learns better than SARSA
* Optimistic selection selects the best action for learning